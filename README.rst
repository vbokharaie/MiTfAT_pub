MiTfAT
======

.. image:: https://img.shields.io/pypi/v/MiTfAT.svg
    :target: https://pypi.python.org/pypi/MiTfAT
    :alt: Latest PyPI version
.. image:: https://zenodo.org/badge/203363866.svg
   :target: https://zenodo.org/badge/latestdoi/203363866
.. image:: https://img.shields.io/badge/License-GPLv3-blue.svg
   :target: https://www.gnu.org/licenses/gpl-3.0


Introduction
------------

`MiTfAT` is a scikit-learn-friendly Python library to analyse fMRI data.

There are a few software packages that are commonly used by researchers to pre-process the fMRI time-series and then analyse them. But the decision to develop a new library which eventually led to `MiTfAT` library is motivated by a few reasons. The main reason is that all the commonly used fMRI analysis packages come with so many belts and whistles that it usually takes the user a long time to find out which parts of each software package is what he/she need. Another reason was that none of the commonly used software packages I could find was written in Python, which is my programming language of choice. And lastly, the analysis methods I needed for the problems I had in mind were not available in any of those software packages.

Hence the `MiTfAT` was developed. It is designed to be used for general fMRI time-series analysis, but in particular, signals obtained from molecular fMRI studies, i.e. the cases in which we measure the changes in concentration of molecules which might have been directly injected into the brain (which happens when the molecule is too big to pass through blood-brain-barrier).

The basic principle behind `MiTfAT` is that it imports all the relevant data of an fMRI experiment into a class object. The fMRI time-series are a member of this class and are stores as NumPy arrays. There are various functionalities available to analyse the data in a number of ways. They include:

- Clustering the time-series using K-means clustering. Clustering can be done based on values in all time-steps, or the mean value of each time-series or slope of a linear-regression passing through the time-series. And it can be applied to raw data or the normalised data. `MiTfAT` uses `scikit-learn` for all machine learning functions. Hence, K-means can be easily changed with any other clustering algorithm implemented in `scikit-learn`.

- Hierarchical clustering; which applies clustering in two stages. In the first step, the algorithm removes the time-series corresponding to voxels in which signal to noise ratio is not high enough. And in the second step, the time-series corresponding to the remaining voxels are clustered.

- Detrending. We can remove the general trends in time-series to make the transient changes more visible. As an example, if we cannot wait long enough until the concentration of our agent reaches steady-state level then transient variations in the signal caused by changes in experimental conditions, which is what we are interested in, might be obscured by such trends in the signal. `MiTfAT` can detrend the time series and give us a signal which looks like the one we might expect in steady-state. And then, transient changes due to experimental design would be quantifiable.

- Interpolation of time-series under varying conditions. Let's say we have an experiment in which the total recording time is divided into 3 segments. In the second segment, we record under a different condition compared to the first and last segment. This can be, as an example, the occlusion of an artery which changes calcium concertation in the brain when the agent we have injected into the brain binds with calcium. Obviously, we want to quantify how the signal has changed in the second segment. In order to do so, we should interpolate the time-series in segment 2 based on values of segments 1 and 3, and then compare it with the actual measurements. `MiTfAT` provides such a feature for us.

- Averaging over many trials. If we repeat an experiment many times, then it is usually of interest to average the measurements over all the trials. This can be useful if each measurement is noisy and we want to attenuate the effects of noise. `MiTfAT` allows us to do so.

Installation
------------
You can install `MitfAT` using `pip`. In your command prompt or bash, simply type:

 .. code-block:: bash

    pip install mitfat

If you are using Anaconda on Windows, better to open an Anaconda command prompt
and then type the above.

Or if you want to work with the latest beta-release, you can install directly from this repository.


    **If you don't know anything about Python:**

    then you should not worry. In order to use this code, you do not need to know anything about python. You just need to install it and then follow the instructions in the Usage section to be able to run the code. But you need to install Python first. Python is just a programming language and unlike Matlab is not owned by a company or organization, hence you do not need to bu a license to use it. There are various ways to install Python, but the easiest is to go `here <https://docs.conda.io/en/latest/miniconda.html>`_ and install Miniconda Python 3.x (3.7 at the time of writing). This will install Python and a minimal set of commonly used libraries (Libraries in Python are equivalent to toolboxes in Matlab). A little detail to keep in mind for Windows users is that you need to open an Anaconda Prompt (which you can find in your Start menu) and then type ``pip install mitfat`` to install the MitfAT library. Typing it in a normal windows command prompt (which you can open by typing ``cmd`` in 'Search program or file' in Start menu) might not work properly.

    When Python is installed, then follow the instructions below to use this code to analyse your fMRI data. I should add though, that I sincerely hope using this code can motivate you to learn a bit about Python. I learned how to use Matlab 20 years ago and still use it to this day. But as I learn more about Python and what is available in this ecosystem, I use Matlab less and Python more and more every day. Python provides powerful tools for you that you did not know you are missing when you were writing programs in Matlab. If you want to learn the basics of Python, I can suggest this `online book <https://jakevdp.github.io/PythonDataScienceHandbook/>`_ to start with.


Usage
-----

In the 'docs' folder of the repository, you can find the `User Manual <docs/mitfat.pdf>`_, which includes detailed description of the various functionalities of the `MiTfAT` library, and example.

Thee are also two scripts in `tests` folder, accomponied with sample datasets, which you can run to see sample outputs of the library. 



Requirements
^^^^^^^^^^^^

 .. code-block:: python

  "pandas",
  "numpy",
  "scipy",
  "matplotlib",
  "nibabel",
  "nilearn",
  "pathlib",
  "seaborn",
  "openpyxl",


Compatibility
-------------

This code is tested under Python 3.8, and should work well for all current versions of Python 3.

Licence
-------
GNU General Public License (Version 3).

Citation
--------
Please use the CITATION.cff file.

This code was originally developed for a collaboration which led to the following publications:

Savić T. , Gambino G., Bokharaie V. S., Noori H. R., Logothetis N.K., Angelovski G., "Early detection and monitoring of cerebral ischemia using calcium-responsive MRI probes", PNAS, 2019.

Author
-------

`MiTfAT` is maintained by `Vahid Samadi Bokharaie <vahid.bokharaie@protonmail.com>`_.
